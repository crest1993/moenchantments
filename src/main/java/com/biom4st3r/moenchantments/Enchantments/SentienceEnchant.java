package com.biom4st3r.moenchantments.Enchantments;

import com.biom4st3r.moenchantments.MoEnchantmentsMod;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.chat.BaseComponent;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.server.command.ServerCommandSource;

public class SentienceEnchant extends MoEnchant {

    public SentienceEnchant()
    {
        super(Weight.COMMON, EnchantmentTarget.ALL, new EquipmentSlot[]{EquipmentSlot.MAINHAND});
        //Enchantments
    }
    @Override
    public String getDisplayName() {
        return "Sentience";
    }

    @Override
    public String regName() {
        return "sentience";
    }

    @Override
    public int getMaximumLevel() {
        return 1;
    }

    @Override
    public int getMinimumLevel() {
        return 1;
    }

    @Override
    public boolean isAcceptableItem(ItemStack iS) {
        //return true;
        return this.type.isAcceptableItem(iS.getItem());
    }
    
    @Override
    public boolean isTreasure() {
        return true;
    }

    @Override
    public boolean enabled() {
        return MoEnchantmentsMod.config.EnableSentience;
    }

    public void m(ServerCommandSource source, BaseComponent message)
    {
        source. sendFeedback(message, true);
    }

    public void p(PlayerEntity player, BaseComponent message)
    {
        player.sendMessage(message);
    }
    /*
    public static BaseComponent c(Object ... fields)
    {
        BaseComponent message = new TextComponent("");
        BaseComponent previous_component = null;
        for (Object o: fields)
        {
            if (o instanceof BaseComponent)
            {
                message.append((BaseComponent)o);
                previous_component = (BaseComponent)o;
                continue;
            }
            String txt = o.toString();
            BaseComponent comp = _getChatComponentFromDesc(txt,previous_component);
            if (comp != previous_component) message.append(comp);
            previous_component = comp;
        }
        return message;
    }
    
    public static void send(PlayerEntity player, TextComponent message)
    {
        lines.forEach(player::sendMessage);
    }
    */
}