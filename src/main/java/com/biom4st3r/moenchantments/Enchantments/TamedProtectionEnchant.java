package com.biom4st3r.moenchantments.Enchantments;

import com.biom4st3r.moenchantments.MoEnchantmentsMod;

import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.AxeItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.SwordItem;

//credit electric_necropolis on reddit
public class TamedProtectionEnchant extends MoEnchant {

    public TamedProtectionEnchant()
    {
        super(Weight.COMMON,EnchantmentTarget.WEAPON,new EquipmentSlot[] {EquipmentSlot.MAINHAND});
    }

    @Override
    public String getDisplayName() 
    {
        return "Familiarity";
    }

    @Override
    public String regName() 
    {
        return "tamedprotection";
    }

    @Override
    public int getMaximumLevel() 
    {
        return 1;
    }

    @Override
    public int getMinimumLevel() 
    {
        return 1;
    }

    @Override
    public boolean isAcceptableItem(ItemStack iS) 
    {
        return iS.getItem() instanceof AxeItem || iS.getItem() instanceof SwordItem;
    }

	@Override
    public boolean isTreasure() 
    {
		return true;
    }

    @Override
    public boolean enabled() {
        return MoEnchantmentsMod.config.EnableTamedProtection;
    }
    
}