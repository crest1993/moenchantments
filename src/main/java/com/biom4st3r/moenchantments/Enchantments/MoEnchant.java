package com.biom4st3r.moenchantments.Enchantments;

import java.util.TreeMap;
import net.minecraft.ChatFormat;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;

public abstract class MoEnchant extends Enchantment {

    private final static TreeMap<Integer, String> map = new TreeMap<Integer, String>();

    public MoEnchant(Weight w, EnchantmentTarget et, EquipmentSlot[] es) 
    {
        super(w, et, es);
    }

    public abstract String getDisplayName();

    public abstract String regName();

    @Override
    public Component getTextComponent(int level) 
    {
        Component component_1 = new TranslatableComponent(this.getTranslationKey(), new Object[0]);
        if (this.isCursed()) {
            component_1.applyFormat(ChatFormat.RED);
        } else {
            component_1.applyFormat(ChatFormat.GRAY);
        }
        if (level != 1 || this.getMaximumLevel() != 1) {
            component_1.append(" ").append(new TranslatableComponent(toRoman(level)));
        }
        return component_1;
    }

    public abstract boolean enabled();

    @Override
    public abstract int getMaximumLevel();

    @Override
    public abstract int getMinimumLevel();

    public abstract boolean isAcceptableItem(ItemStack iS);

    public static String toRoman(int num) {
        int l = map.floorKey(num);
        if (num == l) {
            return map.get(num);
        }
        return map.get(l) + toRoman(num - l);
    }

    // #region Statics
    static {
        map.put(1000, "M");
        map.put(900, "CM");
        map.put(500, "D");
        map.put(400, "CD");
        map.put(100, "C");
        map.put(90, "XC");
        map.put(50, "L");
        map.put(40, "XL");
        map.put(10, "X");
        map.put(9, "IX");
        map.put(5, "V");
        map.put(4, "IV");
        map.put(1, "I");

    }
    // #endregion

    public static boolean hasEnchant(Enchantment e, ItemStack i) {
        return EnchantmentHelper.getLevel(e, i) > 0;
    }
    
}