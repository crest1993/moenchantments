package com.biom4st3r.moenchantments.mixin;

import java.util.List;

import com.biom4st3r.moenchantments.Enchantments.MoEnchant;
import com.biom4st3r.moenchantments.interfaces.PotionEffectRetainer;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import net.minecraft.ChatFormat;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.chat.TranslatableComponent;


@Mixin(ItemStack.class)
public abstract class ItemStackClientMixin
{
    @Inject(at = @At(value = "TAIL"),method = "getTooltip",cancellable = false)    
    public void getTooltipText(PlayerEntity playerEntity_1, TooltipContext tooltipContext_1,CallbackInfoReturnable<List<Component>> ci) 
    {
        List<Component> c = ci.getReturnValue();
        if(((PotionEffectRetainer) this).isPotionRetainer() > 0 && ((PotionEffectRetainer) this).getCharges() > 0)
        {
            for(int i = 0; i < c.size(); i++)
            {
                if(c.get(i) instanceof TranslatableComponent)
                {
                    if(((TranslatableComponent)c.get(i)).getKey().contains("potionretension"))
                    {
                        String name = new TranslatableComponent(((PotionEffectRetainer) this).getEffect().getTranslationKey(), new Object[0]).getFormattedText();
                        Component text = new TextComponent(
                            String.format(
                                "%s- %s %s%s %s/%s",
                                ChatFormat.GRAY,
                                name,
                                MoEnchant.toRoman(((PotionEffectRetainer)this).getAmplification()+1),
                                ChatFormat.RESET,
                                ((PotionEffectRetainer) this).getCharges(),
                                ((PotionEffectRetainer) this).getMaxCharges()
                                ));
                        c.add(i+1,text);
                    }
                }
            }
        }
    }
}