package com.biom4st3r.moenchantments.entities;

import com.biom4st3r.moenchantments.MoEnchantmentsMod;
import com.biom4st3r.moenchantments.MoEnchants;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.damage.ProjectileDamageSource;
import net.minecraft.entity.projectile.AbstractFireballEntity;
import net.minecraft.entity.projectile.FireballEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.particle.ParticleEffect;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.util.hit.EntityHitResult;
import net.minecraft.util.hit.HitResult;
import net.minecraft.util.hit.HitResult.Type;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class HerosProjectileEntity extends AbstractFireballEntity {
    private ItemStack sourceWeapon;

    public HerosProjectileEntity(World w) {
        super(MoEnchantmentsMod.herosProjectile, w);
    }

    @Environment(EnvType.CLIENT)
    public HerosProjectileEntity(World world_1, double x, double y, double z, double pitchMaybe, double yawMaybe,
            double velocityMaybe) {

        super(MoEnchantmentsMod.herosProjectile, x, y, z, pitchMaybe, yawMaybe, velocityMaybe, world_1);
    }

    public HerosProjectileEntity(World w, LivingEntity sender, double x, double y, double z) {
        super(MoEnchantmentsMod.herosProjectile, sender, x, y, z, w);
        if (EnchantmentHelper.getLevel(MoEnchants.HEROSSWORD, sender.getMainHandStack()) > 0) {
            sourceWeapon = sender.getMainHandStack();
        }
        // this.applyAngularVelcoity(sender, sender.pitch, sender.yaw, 0.0f);
    }

    public void applyAngularVelcoity(Entity source, float pitch, float yaw, float yModifier) {// , float float_4, float
                                                                                              // float_5) {
        float xVelocity = -MathHelper.sin(yaw * 0.017453292F) * MathHelper.cos(pitch * 0.017453292F);
        float yVelocity = -MathHelper.sin((pitch + yModifier) * 0.017453292F);
        float zVelocity = MathHelper.cos(yaw * 0.017453292F) * MathHelper.cos(pitch * 0.017453292F);
        this.setVelocity((double) xVelocity, (double) yVelocity, (double) zVelocity);// , float_4, float_5);
        Vec3d vec3d_1 = source.getVelocity();
        this.setVelocity(this.getVelocity().add(vec3d_1.x, source.onGround ? 0.0D : vec3d_1.y, vec3d_1.z));
    }

    @Override
    public void tick() {
        System.out.println(String.format("%.02f, %.02f, %.02f", this.x, this.y, this.z));
        if (this.owner != null && this.distanceTo(this.owner) > 8 * 10) {
            this.remove();
        } else if (this.owner == null) {
            this.remove();
        }
        super.tick();
    }

    @Override
    protected void onCollision(HitResult hit) 
    {
        if (!this.world.isClient)
        {
            if (hit.getType() == Type.ENTITY)
            {
                EntityHitResult ehit = (EntityHitResult) hit;
                LivingEntity defender = (LivingEntity) ehit.getEntity();
                defender.damage(HeroProjectileDamageSource.create(this.owner, defender, sourceWeapon), this.sourceWeapon.getDamage() / 2);
                float knockback = EnchantmentHelper.getLevel(Enchantments.KNOCKBACK, sourceWeapon) * 0.4f;// vanilla for knockback is 0.5f
                defender.takeKnockback(
                    this.owner, 
                    (knockback > 0 ? knockback : 0.4f), 
                    (double) MathHelper.sin(this.yaw * 0.017453292F),
                    (double) (-MathHelper.cos(this.yaw * 0.017453292F))
                    );
            }
            if (hit.getType() == Type.BLOCK) 
            {
                this.remove();
            }
        }
    }

    @Override
    protected ItemStack getItem() 
    {
        return new ItemStack(MoEnchantmentsMod.dhpi, 1);
    }

    @Override
    @Environment(EnvType.CLIENT)
    public ItemStack getStack() 
    {
        return new ItemStack(MoEnchantmentsMod.dhpi, 1);
    }

    @Override
    protected ParticleEffect getParticleType() 
    {
        return ParticleTypes.ENCHANT;
    }
}

class HeroProjectileDamageSource extends ProjectileDamageSource
{
    public HeroProjectileDamageSource(Entity attacker, Entity defender, ItemStack iS)
    {
        super("heroslice",attacker,defender);
        this.setBypassesArmor();
        if(EnchantmentHelper.getLevel(Enchantments.FIRE_ASPECT, iS) > 0)
        {
            this.setFire();
        }
    }

    public static DamageSource create(Entity attacker, Entity defender,ItemStack iS)
    {
        return new HeroProjectileDamageSource(attacker, defender, iS);
    }
}