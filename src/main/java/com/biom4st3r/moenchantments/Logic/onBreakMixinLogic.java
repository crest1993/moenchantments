package com.biom4st3r.moenchantments.Logic;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import com.biom4st3r.moenchantments.MoEnchantmentsMod;
import com.biom4st3r.moenchantments.MoEnchants;
import com.biom4st3r.moenchantments.util;

import net.minecraft.ChatFormat;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.CropBlock;
import net.minecraft.block.LogBlock;
import net.minecraft.block.OreBlock;
import net.minecraft.block.RedstoneOreBlock;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.ExperienceOrbEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;

public class onBreakMixinLogic
{

    public static int cap(int val, int ceiling)
	{
		return val > ceiling ? ceiling : val;
	}

	public static void tryVeinMining(World world,BlockPos blockPos,BlockState blockState,PlayerEntity pe)
    {
		if(world.isClient)
		{
			return;
		}
		ItemStack tool = pe.getMainHandStack();
		if(tool == ItemStack.EMPTY || !tool.hasEnchantments())
		{
			return;
		}
		boolean hasAutoSmelt = false;

		int VeinMinerLvl = EnchantmentHelper.getLevel(MoEnchants.VEINMINER, tool);
		int TreeFellerLvl = EnchantmentHelper.getLevel(MoEnchants.TREEFELLER,tool);
		//int havesterLvl = EnchantmentHelper.getLevel(MoEnchants.HAVESTER,tool);
		hasAutoSmelt = EnchantmentHelper.getLevel(MoEnchants.AUTOSMELT, tool) > 0;

		Block currentType = blockState.getBlock();
		int brokenBlocks = 0;

		if(TreeFellerLvl > 0)
        {
            if((currentType instanceof LogBlock))
            {
				brokenBlocks = doVeinMiner(blockState,world,blockPos,MoEnchantmentsMod.config.TreeFellerMaxBreakByLvl[TreeFellerLvl-1],pe,tool);
				util.Debug("onBreakMixinLogin#tryVeinMining", brokenBlocks + " block broken");
			}
		}
		else if(VeinMinerLvl > 0)
		{
			if(isValidVeiningBlock(currentType))
			{
				brokenBlocks = doVeinMiner(blockState,world,blockPos,MoEnchantmentsMod.config.VeinMinerMaxBreakByLvl[VeinMinerLvl-1],pe,tool);
				util.Debug("onBreakMixinLogin#tryVeinMining", brokenBlocks + " block broken");
			}
		}
		if(hasAutoSmelt)
		{
			//world.playSound(this.x, this.y, this.z, SoundEvents.AMBIENT_UNDERWATER_ENTER, SoundCategory.AMBIENT, 1.0F, 1.0F, false);
			if(tool.getTag().containsKey("smeltexperience"))
			{
				float exp = tool.getTag().getFloat("smeltexperience");
				if(exp > 1)
				{
					tool.getTag().putFloat("smeltexperience", 0);
					for(;exp > 1; exp--)
					{
						dropExperience(1f, blockPos, world);
					}
				}
			}
		}
	}

	public static ArrayList<CropBlock> getCropBlocks(World w, BlockPos source, Block type)
	{
		return null;
	}
	
    public static ArrayList<BlockPos> getSameBlocks(World w,BlockPos source, Block type)
    {
        ArrayList<BlockPos> t = new ArrayList<BlockPos>();
		for(BlockPos pos : new BlockPos[] {source.up().south(),source.up().east(),source.up().west(),source.up().north(),
											source.up(),source.down(),source.north(),source.east(),source.south(),source.west(),
											source.down().north(),source.down().east(),source.down().west(),source.down().south()})
        {
            if(w.getBlockState(pos).getBlock() == type)
            {
                t.add(pos);
            }
		}
		
        return t;
	}

	private static boolean isValidVeiningBlock(Block b)
	{
		if(b == Blocks.STONE || b == Blocks.ANDESITE || b == Blocks.GRANITE || b == Blocks.DIORITE)
		{

			return false;
		}
		if(b instanceof OreBlock)
		{
			return true;
		}
		else if(b instanceof RedstoneOreBlock)
		{
			return true;
		}

		else if(MoEnchantmentsMod.class_whitelist.contains(b.getClass()))
		{
			util.Debug("onBreakMixinLogin#isValiVeiningBlock; class_whitelist", b.toString() + " true");
			return true;
		}
		else if(MoEnchantmentsMod.block_whitelist.contains(b))
		{
			util.Debug("onBreakMixinLogin#isValiVeiningBlock; block_whitelist", b.toString() + " true");
			return true;
		}
		util.Debug("onBreakMixinLogin#isValiVeiningBlock", b.toString() + " global false");
		return false;
	}
	
	private static void dropExperience(float experienceValue ,BlockPos blockPos,World world)
	{
		world.spawnEntity(new ExperienceOrbEntity(world, blockPos.getX(), blockPos.getY(), blockPos.getZ(), (int)experienceValue));
	}

	private static int doVeinMiner(BlockState blockState,World world,BlockPos blockPos,int maxBlocks,PlayerEntity pe,ItemStack tool)
	{
		util.Debug("onBreakMixinLogic#doVeinMiner", blockState.getBlock());
		//ItemStack recipeResult = recipe.isPresent() ? recipe.get().getOutput().copy() : ItemStack.EMPTY;
		int blocksBroken = 0;
		//float experiencetotal = 0.0f;
		Queue<BlockPos> toBreak = new LinkedList<BlockPos>();
		toBreak.addAll(getSameBlocks(world,blockPos,blockState.getBlock()));
		System.out.println(String.format("Max Dam: %s\nCur Dam: %s\n %s", tool.getMaxDamage(),tool.getDamage(),tool.getMaxDamage()-tool.getDamage()));
		while(!toBreak.isEmpty() && blocksBroken <= (maxBlocks))
		{
			util.Debug("onBreakMixinLogic#doVeinMiner", toBreak.size() + " Blocks to break");
			util.Debug("onBreakMixinLogic#doVeinMiner", blocksBroken + "/" + maxBlocks);
			BlockPos currPos = toBreak.remove();
			double distance = pe.getBlockPos().getSquaredDistance(new Vec3i(currPos.getX(),currPos.getY(),currPos.getZ()));
			
			if(toBreak.size() < 50)
				toBreak.addAll(getSameBlocks(world, currPos, blockState.getBlock()));

			if(Math.sqrt(distance) <= MoEnchantmentsMod.config.MaxDistanceFromPlayer && world.getBlockState(currPos).getBlock() == blockState.getBlock())
			{
				util.Debug("onBreakMixinLogic#doVeinMiner", "valid Block");
				Block.dropStacks(world.getBlockState(currPos), world, pe.getBlockPos(), null, pe, tool);
				world.breakBlock(currPos, false);
				
				tool.damage(1, pe,(playerEntity_1) -> {
					playerEntity_1.sendToolBreakStatus(pe.getActiveHand());
				});
				if((MoEnchantmentsMod.config.ProtectItemFromBreaking ? tool.getMaxDamage()-tool.getDamage() < 10 : true))
				{
					pe.playSound(SoundEvents.ENTITY_ITEM_BREAK, SoundCategory.PLAYERS, 1f, 1f);
					pe.sendMessage(new TranslatableComponent("My pick is badly damaged").applyFormat(ChatFormat.GREEN));
					break;
				}
				blocksBroken++;
			}
			world.setBlockState(currPos, Blocks.AIR.getDefaultState());
		}
		pe.addExhaustion(0.005F * blocksBroken);
		return blocksBroken;
	}
}