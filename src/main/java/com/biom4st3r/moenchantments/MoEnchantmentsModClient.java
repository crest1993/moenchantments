package com.biom4st3r.moenchantments;


import com.biom4st3r.moenchantments.entities.HerosProjectileEntity;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.render.EntityRendererRegistry;
import net.minecraft.client.render.entity.FlyingItemEntityRenderer;

public class MoEnchantmentsModClient implements ClientModInitializer {

    @Override
    public void onInitializeClient() {
        // ClientSidePacketRegistry.INSTANCE.register(MISSEDATTACK, consumer);
        // EntityRendererRegistry.INSTANCE.register(
        // HerosProjectile.class,
        // ((entityRenderDispatcher, context) -> new
        // HerosProjectileRender<HerosProjectile>(entityRenderDispatcher,context.getItemRenderer())));

        EntityRendererRegistry.INSTANCE.register(HerosProjectileEntity.class, ((dispatcher,
                context) -> new FlyingItemEntityRenderer<HerosProjectileEntity>(dispatcher, context.getItemRenderer())
  
            ));
    }

}