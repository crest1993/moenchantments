package com.biom4st3r.moenchantments;

public class MoEnchantsConfig
{
    public boolean EnableAutoSmelt ;
    public boolean EnableEnderProtection ;
    public boolean EnablePotionRetention ;
    public boolean EnableTamedProtection ;
    public boolean EnableTreeFeller ;
    public boolean EnableVeinMiner ;
    public boolean EnableSentience;

    public int[] VeinMinerMaxBreakByLvl;

    public int[] TreeFellerMaxBreakByLvl;
    
    public int MaxDistanceFromPlayer;

    public boolean ProtectItemFromBreaking;

    public float AutoSmeltWoodModifier;

    public boolean TameProtectsOnlyYourAnimals;

    //public boolean vanillaToolTips = false;

    public int chanceForEnderCurseToTeleport;
    public int chanceForEnderCurseToPreventDamage;

    public int perLevelChargeMultiplierForPotionRetention;
    public int PotionRetentionMaxLevel;

    public String[] veinMinerBlockWhiteList;
    public String[] veinMinerClassWhiteList;

    public String[] AutoSmeltBlackList;

    public MoEnchantsConfig()
    {
        VeinMinerMaxBreakByLvl = new int[] {7,14,28};
        TreeFellerMaxBreakByLvl = new int[] {14,28,56};
        MaxDistanceFromPlayer = 8;
        ProtectItemFromBreaking = true;

        AutoSmeltWoodModifier = 0.15f;
        TameProtectsOnlyYourAnimals = true;
        chanceForEnderCurseToPreventDamage = 20;
        chanceForEnderCurseToTeleport = 40;
        perLevelChargeMultiplierForPotionRetention = 5;
        PotionRetentionMaxLevel = 10;
        veinMinerBlockWhiteList = new String[] {
            "minecraft:obsidian",
            "netherthings:nether_coal_ore",
            "netherthings:nether_iron_ore",
            "netherthings:nether_gold_ore",
            "netherthings:nether_redstone_ore",
            "netherthings:nether_lapis_ore",
            "netherthings:nether_emerald_ore",
            "netherthings:glowstone_ore",
            "netherthings:quartz_ore",
            "netherthings:nether_vibranium_ore",
            "refinedmachinery:copper_ore",
            "refinedmachinery:lead_ore",
            "refinedmachinery:silver_ore",
            "refinedmachinery:tin_ore",
            "refinedmachinery:nickel_ore"
        };
        veinMinerClassWhiteList = new String[] {
            "com.brand.netherthings.blocks.BlockOreDiamondP",
            "abused_master.superores.blocks.BlockOreBase",
            "io.github.cottonmc.resources.LayeredOreBlock"
        };
        EnableAutoSmelt = true;
        EnableEnderProtection = true;
        EnablePotionRetention = true;
        EnableTamedProtection = true;
        EnableTreeFeller = true;
        EnableVeinMiner = true;
        EnableSentience = true;
        AutoSmeltBlackList = new String[]
        {
            "minecraft:stone"
        };
        
    }




    






}